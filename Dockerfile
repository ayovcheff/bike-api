FROM node:16
ENV NODE_ENV production
# Add a work directory
WORKDIR /app
# Cache and Install dependencies
COPY package.json .
COPY config .

RUN yarn install
RUN ls
# Copy app files
COPY . .
# RUN yarn run migrate -f ./config/npg-migrate.json

# Expose port
EXPOSE 4000
# Start the app
CMD [ "yarn", "start" ] 