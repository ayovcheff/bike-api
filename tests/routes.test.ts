import * as request from 'supertest';
import { server } from '../src/app';

jest.useFakeTimers();

describe("GET /", () => {
  it("returns status code 200 when request is send to base endpoint", async () => {
    request(server).get("/")
      .expect(200)
      .then((res) => {
        expect(res.statusCode).toEqual(200);
        expect(res.body).toBeTruthy();
        expect(res.body).toEqual({msg: 'Hello World!'});
      });
  });
});

describe("POST /user", () => {
	it("returns status code 200 if user is created", async () => {
    const newUser = {
      name: 'James Bond',
      email: 'jamesb@test.co.uk',
      password: '1234',
    };

    const result =  await request(server)
      .post("/user")
      .send(newUser);

    expect(result.statusCode).toEqual(200);
  });
});

describe("GET /token", () => {
	it("returns status code 401 if token is not provided", async () => {
    const result =  await request(server)
      .get("/token")

    expect(result.statusCode).toEqual(401);
    expect(result.body.msg).toEqual('Token not provided');
  });

  it("returns status code 401 if token is not valid", async () => {
    const result =  await request(server)
      .get("/token")
      .set({ Authorization: 'fake token' });

    expect(result.statusCode).toEqual(401);
    expect(result.body.msg).toEqual('Invalid token');
  });
});