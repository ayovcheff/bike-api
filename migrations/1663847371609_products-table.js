/* eslint-disable camelcase */

exports.shorthands = undefined;

exports.up = pgm => {
  pgm.createExtension('uuid-ossp', {
    ifNotExists: true,
  });
  pgm.createTable('products', {
    id: { type: 'uuid', default: pgm.func('uuid_generate_v4 ()'), primaryKey: true},
    model: { type: 'varchar(1000)', notNull: true },
    description: { type: 'varchar(1000)', notNull: true },
    color: { type: 'varchar(1000)', notNull: true },
    price: { type: 'integer', notNull: true },
    size: { type: 'varchar(1000)', notNull: true },
    images: { type: 'varchar ARRAY', notNull: true, default: '{}' },
    createdAt: {
      type: 'timestamp',
      notNull: true,
      default: pgm.func('current_timestamp'),
    },
  })
};

exports.down = pgm => {
  pgm.dropTable('products', {
    ifExists: true
  });
};
