/* eslint-disable camelcase */

exports.shorthands = undefined;

exports.up = pgm => {
  pgm.createExtension('uuid-ossp', {
    ifNotExists: true,
  });
  pgm.createTable('users', {
    id: { type: 'uuid', default: pgm.func('uuid_generate_v4 ()'), primaryKey: true },
    name: { type: 'varchar(1000)', notNull: true },
    email: { type: 'varchar(1000)', notNull: true, unique: true },
    password: { type: 'varchar(1000)', notNull: true },
    createdAt: {
      type: 'timestamp',
      notNull: true,
      default: pgm.func('current_timestamp'),
    },
  })
};

exports.down = pgm => {
  pgm.dropTable('users', {
    ifExists: true
  });
};
