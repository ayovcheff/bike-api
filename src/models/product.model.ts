import {
  IsString,
  IsNumber,
  IsDate,
  IsOptional,
  validate,
} from 'class-validator';
import { QueryConfig } from 'pg';
import { IProduct } from '../interfaces/common/product.interface';

export class Product {
  constructor(productData: IProduct) {
    this.model = productData.model;
    this.price = productData.price;
    this.description = productData.description;
    this.color = productData.color;
    this.size = productData.size;
    this.images = productData.images;
  }

  @IsString()
  model: string;

  @IsString()
  description: string;

  @IsNumber()
  price: number;

  @IsString()
  color: string;

  @IsString()
  size: string;

  @IsString({each: true})
  images: string[];

  @IsOptional()
  @IsDate()
  createdAt: string;

  validate() {
    return validate(this);
  }
}

export const createProductQuery = ({model, price, description, size, color, images}: IProduct): QueryConfig => ({
  text: 'INSERT INTO products(model, price, description, size, color, images) VALUES($1, $2, $3, $4, $5, $6) RETURNING *;',
  values: [model, price, description, size, color, images],
});

export const editProductQuery = ({model, price, description, size, color, images, id}: IProduct): QueryConfig => ({
  text: 'UPDATE products SET model = ($1), price = ($2), description = ($3), size = ($4), color = ($5), images = ($6) WHERE id = ($7);',
  values: [model, price, description, size, color, images, id],
});

export const getProductQuery = (id: string): QueryConfig => ({
  text: 'SELECT * FROM products WHERE id = ($1);',
  values: [id],
});

export const deleteProductQuery = (id: string): QueryConfig => ({
  text: 'DELETE FROM products WHERE id = ($1);',
  values: [id],
});

export const getProductsQuery = (): QueryConfig => ({
  text: 'SELECT * FROM products;'
});
