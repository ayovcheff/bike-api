import {
  IsString,
  IsEmail,
  IsDate,
  IsOptional,
  validate,
} from 'class-validator';
import { QueryConfig } from 'pg';
import { IUser } from '../interfaces/common/user.interface';

export class User {
  constructor(userData: IUser) {
    this.name = userData.name;
    this.email = userData.email;
    this.password = userData.password;
  }

  @IsString()
  name: string;

  @IsEmail()
  email: string;

  @IsString()
  password: string;

  // TODO: when user is created, user email should be verified
  // @IsBoolean()
  // emailVerified: boolean;

  @IsOptional()
  @IsDate()
  createdAt: string;

  validate() {
    return validate(this);
  }
}

export const createUserQuery = ({name, email, password}: IUser): QueryConfig => ({
  text: 'INSERT INTO users(name, email, password) VALUES($1, $2, $3) RETURNING *',
  values: [name, email, password],
});

export const getUserQuery = (id: String): QueryConfig => ({
  text: `SELECT * FROM users WHERE id = ${id};`,
});

export const getUserByEmailQuery = (email: String): QueryConfig => ({
  text: 'SELECT * FROM users WHERE email = ($1);',
  values: [email],
});
