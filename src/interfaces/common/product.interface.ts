export interface IProduct {
  model: string;
  description: string;
  price: number;
  id?: string;
  color: string;
  size: string;
  images: string[];
}