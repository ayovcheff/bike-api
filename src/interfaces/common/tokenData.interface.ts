import { JwtPayload } from "jsonwebtoken";

export interface ITokenData extends JwtPayload{
  iat: number;
  exp: number;
  id: string;
  name: string;
  email: string;
  isPremium: boolean;
}
