import { Application, Request } from 'express';
import { IUser } from './user.interface';

export interface IApplication extends Application {
// tslint:disable-next-line: no-any
  [key: string]: any;
}

export interface IRequest extends Request {
  user: IUser;
}
