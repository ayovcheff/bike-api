import { Request, Response, NextFunction  } from 'express';
import { IRequest } from './common/application.interface';

export interface IRoutes {
  root: string;
  middleware?: IMiddleware[];
  routes: IRoute[];
  version?: string;
}

interface IRoute {
  method: string;
  path: string;
  action: (req: Request | IRequest, res: Response, next?: NextFunction) => Promise<Response<any>>;
}

export type IMiddleware  = (req: IRequest | Request, res: Response, next: NextFunction) => void;
