import { Response } from "express";

export const sendBadRequest = (res: Response) => (data: Record<string, any>) =>
  res.status(400).send(data);

export const sendOK = (res: Response) => (data: Record<string, any>) =>
  res.status(200).send(data);

export const sendUnauthorized = (res: Response) => (data?: Record<string, any>) => 
  res.status(401).send(data);