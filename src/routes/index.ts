import { Application, Response, Request } from 'express';
import { routesConfig } from './routes';
import { IRoutes, IMiddleware } from '../interfaces/routes.interface';
import { IApplication } from '../interfaces/common/application.interface';

const registerRoute = (
  app: IApplication,
  method: string,
  path: string,
  action: (req: Request, res: Response) => Promise<Response>,
  middleware: IMiddleware[] = [ ]): void => {
  const actionChain = [...middleware, action];
  app[method](path, ...actionChain);
};

const registerRoutes = (routesDetails: IRoutes[], app: Application): void => {
  routesDetails.forEach(({ root, routes, middleware = []}) => {
    routes.forEach(({ method, path, action }) => {
      registerRoute(app, method, `${root}${path}`, action, middleware);
    });
  });
};

export const initializeRoutes = (app: Application): void => {
  registerRoutes(routesConfig, app);
};
