import { guard } from '../middlewares/guard';
import { IRoutes } from '../interfaces/routes.interface';
import * as userController from '../controllers/user.controller';
import * as productsController from '../controllers/products.controller';
import * as authController from '../controllers/auth.controller';

export const routesConfig: IRoutes[] = [
  {
    root: '/',
    middleware: [],
    routes: [
      // User
      { method: 'get', path: '/', action: async (req, res) => res.status(200).send({ msg: 'Hello World!' })  },
      { method: 'post', path: 'login', action: authController.login },
      { method: 'get', path: 'token', action: authController.checkToken },
      { method: 'post', path: 'user', action: userController.create },
    ],
  },
  {
    root: '/',
    middleware: [guard],
    routes: [
      // User
      { method: 'get', path: 'user/:id', action: userController.read },
      // Product
      { method: 'post', path: 'product', action: productsController.create },
      { method: 'get', path: 'product/:id', action: productsController.read },
      { method: 'post', path: 'product/:id/edit', action: productsController.update },
      { method: 'post', path: 'product/:id/delete', action: productsController.remove },
      { method: 'get', path: 'products', action: productsController.readAll },
      { method: 'post', path: 'upload', action: productsController.upload as any }, 
    ],
  },
];
