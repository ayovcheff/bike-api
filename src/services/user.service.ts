
import { QueryConfig, QueryResult } from 'pg';
import { ifElse } from 'ramda';
// eslint-disable-next-line import/no-cycle
import  appCtx from '../app';
import { Monad } from 'lambda-ts';
import { Left, Right, Either } from 'lambda-ts';
import { IUser } from '../interfaces/common/user.interface';
import { 
  createUserQuery,
  getUserQuery,
  getUserByEmailQuery,
  User,
} from '../models/user.model';

export const createUser = async (user: IUser): Promise<Monad<any>> => {
  const feedback = await new User(user)
    .validate();

  const write = (v: IUser) => Right(v)
    .map(createUserQuery)
    .map((q) => appCtx.db.query(q as QueryConfig));

  const cancel = () => Left(feedback);

  const result: any = ifElse(
    ((errors) => errors.length > 0) as any,
    cancel,
    () => write(user),
  );

  return result(feedback);
};

export const getUser = async (id: string): Promise<Monad<IUser>> => 
  Right(id)
    .map(getUserQuery)
    .map((q: QueryConfig) => appCtx.db.query(q))
    .map((result: QueryResult) => result.rows)
    .map((users: IUser[]) => users[0]) as Monad<IUser>;

export const getUserByEmail = async (email: string): Promise<Monad<IUser>> => {
  const uMonad = Right(email)
    .map(getUserByEmailQuery)
    .map((q: QueryConfig) => appCtx.db.query(q));


  const result = await uMonad.get() as QueryResult<IUser>; 

  return Either(result.rows[0]);
}
