import * as multer from 'multer';
import { uuid } from 'uuidv4';

const createImageName = ({originalname}): string => {
  const ext = originalname.substring(originalname.lastIndexOf('.')+1, originalname.length);
  return `images/${uuid()}.${ext}`;
};

// this is just to test locally if multer is working fine.
const storage = multer.diskStorage({
  destination: (req, res, cb) => {
      cb(null, 'assets')
  },
  filename: (req, file, cb) => {
      cb(null, `images/${uuid()}-${file.originalname}`)
  }
});

const imageTypeValidator = (_, file, cb) => {
  const acceptedImageTypes = ['image/jpg', 'image/jpeg', 'image/png'];
 
  if(!acceptedImageTypes.includes(file.mimetype)){
    return cb(new Error("Invalid file type"), false);
  }
  
  return cb(null, true);
};

export default multer({
  fileFilter: imageTypeValidator,
  storage: storage,
});
