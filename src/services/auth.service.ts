import { IncomingHttpHeaders } from 'http';
import { Either, Monad } from 'lambda-ts';

export const  extractTokenFromAuthHeader = (headers: IncomingHttpHeaders): Monad<string> => 
  Either(headers.authorization)
  .map((value: string) => value.split(' '))
  .map(([_, token]) => token) as Monad<string>;