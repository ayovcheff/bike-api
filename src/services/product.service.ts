
import { QueryConfig, QueryResult } from 'pg';
import { head, ifElse } from 'ramda';
// eslint-disable-next-line import/no-cycle
import  appCtx from '../app';
import { IProduct } from '../interfaces/common/product.interface';
import { Either, Left, Monad, Right } from 'lambda-ts';

import { 
  createProductQuery,
  editProductQuery, 
  getProductQuery,
  deleteProductQuery,
  Product,
  getProductsQuery,
} from '../models/product.model';
import { ValidationError } from 'class-validator';

export const createProduct = async (product: IProduct): Promise<Monad<IProduct | ValidationError[]>> => {
  const feedback = await new Product(product)
    .validate();

  // check if there are validation errors
  if(feedback.length > 0) {
    return Left(feedback);
  }

  const queryResultMonad = Right(product)
    .map(createProductQuery)
    .map((q) => appCtx.db.query(q as QueryConfig));
    
  const result = await queryResultMonad.get() as QueryResult<IProduct>; 

  return Either(result.rows[0]);
};

export const editProduct = async (product: IProduct): Promise<Monad<QueryResult>> => {
  const feedback = await new Product(product)
    .validate();

  const write = (v: IProduct) => Right(v)
    .map(editProductQuery)
    .map((q: QueryConfig) => appCtx.db.query(q));

  const cancel = () => Left(feedback);
  
  // @ts-ignore
  return ifElse(
  // @ts-ignore
    (errors) => errors.length > 0,
    cancel,
    () => write(product),
  // @ts-ignore
  )(feedback);
};

export const getProduct = async (id: string): Promise<Monad<IProduct>> => {
  const queryResultMonad = Right(id)
    .map(getProductQuery)
    .map((q: QueryConfig) => appCtx.db.query(q)) as Monad<QueryResult>;

  const result = await queryResultMonad.get() as QueryResult<IProduct>; 

  return Either(result.rows).map(v => head(v)) as Monad<IProduct>;
} 

export const deleteProduct = async (id: string): Promise<Monad<QueryResult>> => 
  Right(id)
    .map(deleteProductQuery)
    .map((q: QueryConfig) => appCtx.db.query(q)) as Monad<QueryResult>;

export const getProducts = async (): Promise<Monad<IProduct[]>> => {
  const queryResultMonad = Right(getProductsQuery())
    .map((q: QueryConfig) => appCtx.db.query(q)) as Monad<QueryResult>;

  const result = await queryResultMonad.get() as QueryResult<IProduct>; 

  return Either(result.rows);
};