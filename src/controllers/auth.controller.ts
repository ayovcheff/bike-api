import { Request, Response } from "express";
import { sign, verify } from 'jsonwebtoken';
import { TryCatch } from "lambda-ts";
import { equals, pick } from 'ramda';
import { IUser } from "../interfaces/common/user.interface";
import { extractTokenFromAuthHeader } from "../services/auth.service";
import { getUserByEmail } from '../services/user.service';
import config from '../../config';
import { sendBadRequest, sendOK, sendUnauthorized } from "../utils";

const generateAccessToken = (user: IUser): string => {
  const data = pick(['name', 'email', 'id'], user); 
  return sign(data, config.tokenSecret, { expiresIn: '86400s' });
}

export const login = async (req: Request, res: Response): Promise<Response> => {
  const badRequest = sendBadRequest(res);
  const oKRequest = sendOK(res);
  const userMonad = await getUserByEmail(req.body.email);

  // if user is not found return proper response
  if(userMonad.isLeft()) {
    return badRequest({
      msg: 'Wrong credentials',
    });
  }

  const user = userMonad.get();
  const isPasswordMatch = equals(user.password, req.body.password);
  
  // if password doesn't match return proper request
  if(!isPasswordMatch) {
    return badRequest({
      msg: 'Wrong password',
    });
  }

  const token = generateAccessToken(user);
  return oKRequest({token});
}

export const checkToken = async (req: Request, res: Response): Promise<Response> => {
  const badRequest = sendBadRequest(res);
  const oKRequest = sendOK(res);
  const unauthorizedRequest = sendUnauthorized(res);
  const maybeToken = extractTokenFromAuthHeader(req.headers);

  if(maybeToken.isLeft()) {
    return unauthorizedRequest({
      msg: 'Token not provided',
    });
  }

  const maybeIsValid = maybeToken
    .chain((token: string) => TryCatch(() => verify(token, config.tokenSecret)));

  if(maybeIsValid.isLeft()) {
    return unauthorizedRequest({
      msg: 'Invalid token',
    });
  }

  return oKRequest({});
}
