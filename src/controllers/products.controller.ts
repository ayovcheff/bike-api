import { Request, Response } from 'express';
import { ifElse } from 'ramda';

import fileUpload from '../services/image.service';
import { IRequest } from '../interfaces/common/application.interface';
// eslint-disable-next-line import/no-cycle
import { 
  createProduct,
  editProduct,
  getProduct,
  deleteProduct,
  getProducts,
} from '../services/product.service';
import { sendBadRequest, sendOK } from '../utils';

export const create = async (req: IRequest, res: Response): Promise<Response> => {
  const { body, user } = req;
  const { model, color, size, price, description, images } = body;
  const badRequest = sendBadRequest(res);
  const oKRequest = sendOK(res);

  const newProduct = await createProduct({model, price, description, color, size, images });

  return ifElse(
    (u) => u.isLeft(),
    (u) => badRequest({
      msg: 'Product creation failed',
      data: u.get(),  
    }),
    async (u) => oKRequest({
      msg: 'Product created',
      data: await u.get(),
    }),
  )(newProduct);
};

export const read = async (req: Request, res: Response): Promise<Response> => {
  const { id } = req.params;

  const badRequest = sendBadRequest(res);
  const oKRequest = sendOK(res);
  const product = await getProduct(id);
  
  return ifElse(
    (p) => p.isLeft(),
    async (p) => badRequest({
      msg: 'Product get failed',
      data: await p.get(),  
    }),
    async (p) => oKRequest({
      msg: '',
      data: await p.get(),
    }),
  )(product);
};

export const update = async (req: IRequest, res: Response): Promise<Response> => {
  const { description, model, color, size, price, images } = req.body;
  const { id } = req.params;

  const badRequest = sendBadRequest(res);
  const oKRequest = sendOK(res);
  const product = await editProduct({ description, model, color, size, price, id: id.toString(), images });

  return ifElse(
    (p) => p.isLeft(),
    (p) => badRequest({
      msg: 'Product edition failed',
      data: p.get(),  
    }),
    async (p) => oKRequest({
      msg: 'Product edited',
      data: await p.get(),
    }),
  )(product);
};

export const remove = async (req: Request, res: Response): Promise<Response> => {
  const { id } = req.params;

  const badRequest = sendBadRequest(res);
  const oKRequest = sendOK(res);
  const product = await deleteProduct(id);

  return ifElse(
    (p) => p.isLeft(),
    (p) => badRequest({
      msg: 'Product delete failed',
      data: p.get(),  
    }),
    async (p) => oKRequest({
      msg: 'Product deleted',
      data: await p.get(),
    }),
  )(product);
};

export const readAll = async (req: IRequest, res: Response): Promise<Response> => {

  const badRequest = sendBadRequest(res);
  const oKRequest = sendOK(res);
  const products = await getProducts();

  return ifElse(
    (p) => p.isLeft(),
    (p) => badRequest({
      msg: 'There is something wrong with your request',
      data: p.get(),  
    }),
    async (p) => oKRequest({
      msg: '',
      data:   p.get(),
    }),
  )(products);
};

const singleUpload = fileUpload.single("file");
export const upload = async (req: any, res: Response) => {
  const badRequest = sendBadRequest(res);
  const oKRequest = sendOK(res);

  singleUpload(req, res, function (err) {
    const {file} = req;

    if (err) {
      return badRequest({
        msg: 'Image Upload Error',
        data: {
          title: "Image Upload Error",
          detail: err.message,
          error: err,
        },  
      });
    }

    return oKRequest({
      msg: '',
      data:   {uploadURL: `http://127.0.0.1:3006/${file.filename}`},
    });
  });
};
