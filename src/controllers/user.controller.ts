import { Request, Response } from 'express';
import { ifElse } from 'ramda';
// eslint-disable-next-line import/no-cycle
import { 
  createUser,
  getUser,
} from '../services/user.service';
import { sendBadRequest, sendOK } from '../utils';

export const create = async (req: Request, res: Response): Promise<Response> => {
  const { body } = req;
  const { name, email, password } = body;
  const badRequest = sendBadRequest(res);
  const oKRequest = sendOK(res);

  const newUser = await createUser({name, email, password});

  return ifElse(
    (u) => u.isLeft(),
    (u) => badRequest({
      msg: 'User creation failed',
      data: u.get(),  
    }),
    async (u) => oKRequest({
      msg: 'User created successfully',
      data: {},
    }),
  )(newUser);
};

export const read = async (req: Request, res: Response): Promise<Response> => {
  const { id } = req.params;

  const badRequest = sendBadRequest(res);
  const oKRequest = sendOK(res);
  const user = await getUser(id);

  return ifElse(
    (u) => u.isLeft(),
    (u) => badRequest({
      msg: 'User get failed',
      data: u.get(),  
    }),
    async (u) => oKRequest({
      msg: '',
      data: await u.get(),
    }),
  )(user);
};
