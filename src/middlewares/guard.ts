import { Response, NextFunction } from 'express';
import { verify } from 'jsonwebtoken';
import { TryCatch } from 'lambda-ts';
import config from '../../config';
import { IRequest } from '../interfaces/common/application.interface';
import { IUser } from '../interfaces/common/user.interface';
import { extractTokenFromAuthHeader } from '../services/auth.service';

// Authorisation middleware
export const guard = async (
  req: IRequest,
  res: Response,
  next: NextFunction,
  ): Promise<Response | void> => {
  console.log()
  const maybeIsValid = extractTokenFromAuthHeader(req.headers)
    .chain(token => TryCatch(() => verify(token, config.tokenSecret)));

  if(maybeIsValid.isLeft()) {
    return res.sendStatus(401);
  }

  req.user = maybeIsValid.get() as IUser;
  return next();
};
