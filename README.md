# Bike API

Bike API is Node.js, Typescript, PostgreSQL crud example API

## Tech Stack 

* [Doker][docker], [Node.js][node], [Express][express], [Yarn][yarn], [Typescript][typescript], [Lambda TS][lambda-ts] - core platform and dev tools
* [PostgreSQL][pg], [pg][nodepg], [Node pg migrate][node-pg-migrate] — DB, db libs and tools

## Prerequisites

* [Docker][docker] Community Edition v20 or higher

## Getting Started

Clone the repository

```
git@gitlab.com:ayovcheff/bike-api.git
```

Build and run the project

```
cd bike-api
docker-compose up
```

To reach out the API navigate to `http://127.0.0.1:3006`


[yarn]: https://yarnpkg.com/
[typescript]: https://github.com/kriasoft/react-starter-kit
[node]: https://nodejs.org
[express]: http://expressjs.com/
[lambda-ts]: https://github.com/aYo-dev/lambda-ts
[pg]: https://www.postgresql.org/
[nodepg]: https://github.com/brianc/node-postgres
[docker]: https://www.docker.com/community-edition
