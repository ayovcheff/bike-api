import * as dotenv from 'dotenv';
import * as path from 'path';

dotenv.config({path: path.join(__dirname, '..', `.env.${process.env.NODE_ENV}`)});

const config = {
  what: process.env.WHAT || '',
  tokenSecret: process.env.TOKEN_SECRET,
  db: {
    host: process.env.DB_HOST,
    user: process.env.DB_USER,
    database: process.env.DB_NAME,
    password: process.env.DB_PASSWORD,
    port: parseInt(process.env.DB_PORT || "5432"),
    max: parseInt(process.env.DB_MAX as string, 10) || 0,
    idleTimeoutMillis: parseInt(process.env.DB_IDLE_TIMEOUT_MILLIS as string, 10) || 0,
    connectionTimeoutMillis: parseInt(process.env.DB_CONNECTION_TIMEOUT_MILLIS as string, 10) || 0,
  },
  port: parseInt(process.env.PORT as string, 10) || 3000,
  protocol: process.env.PROTOCOL || '',
  host: process.env.HOST || '',
  aws: {
    credentials: {
      accessKeyId: process.env.AWS_ACCESS_KEY,
      secretAccessKey: process.env.AWS_SECRET_ACCESS_KEY,
    },
    bucket: process.env.AWS_BUCKET,
    region: process.env.AWS_REGION,
  },
};

export default config;
